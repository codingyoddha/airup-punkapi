# AirUp PunkIsBeer

## Setup and usage

Run the following DDL statements in PostgresSQL instance
```
CREATE TABLE IF NOT EXISTS punk_is_beer (
   id SMALLINT PRIMARY KEY,
   name VARCHAR(256) NOT NULL,
   tagline VARCHAR(1024),
   first_brewed VARCHAR(10),
   abv FLOAT8
);

CREATE TABLE IF NOT EXISTS food_pairings
(
    id SMALLINT NOT NULL,
    food VARCHAR(512) NOT NULL,
    CONSTRAINT fk_beer
    FOREIGN KEY(id) REFERENCES punk_is_beer(id)
);

```

Create an .env file with the configuration detail for POSTGRESQL instance
```
DBUSER=<USERNAME>
DBPASS=<PASSWORD>
DBHOST=<HOST>
```

Run the program
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

python main.py <NO_OF_BEERS>
```

*Take note, food pairings will have duplicate if this program is run twice. Duplicate checks not implemented.*
