#!/usr/bin/env python3

import requests
import psycopg2
import math
import time
from dotenv import load_dotenv, find_dotenv
import os
import sys

load_dotenv(find_dotenv())

BEER_PER_PAGE_LIMIT = 80              # max 80 from API itself
RATE_LIMIT_KNOCK_OFF_SEC = 0.3        # minimum300 ms between two requests
DB_USER = os.environ.get("DBUSER")
DB_PASS = os.environ.get("DBPASS")
DB_HOST = os.environ.get("DBHOST")
DB_CONN = f"postgres://{DB_USER}:{DB_PASS}@{DB_HOST}"
ORDERS = int(sys.argv[1]) if len(sys.argv) >= 1 else 1


class SoberNotAllowedException(Exception):
    pass


class ThatsNoBeerException(Exception):
    pass


class NoMoreBeerException(Exception):
    pass


class PassedOutException(Exception):
    pass


class BeerIsPunk:
    def __init__(self, rate_limit_knock_off_time):
        self.base_uri = "http://api.punkapi.com/v2/beers/"
        self.last_request_time = 0
        self.rate_limit_knock_off_time = rate_limit_knock_off_time

    def get_beers(self, carton, per_round):
        if carton < 1 or per_round > 80:
            raise ThatsNoBeerException

        # Being polite :)
        # managing 3600 reqs/hr rate limit, knock_off_time determines min time betwee two reqs
        elapsed_time = (time.time() - self.last_request_time)
        if elapsed_time < self.rate_limit_knock_off_time:
            time.sleep(self.rate_limit_knock_off_time - elapsed_time)
        try:
            req = requests.get(
                f"{self.base_uri}?page={carton}&per_page={per_round}")
        except:
            raise PassedOutException
        self.last_request_time = time.time()
        if req.status_code == 200:
            try:
                res = req.json()
                if len(res) < 1:
                    raise NoMoreBeerException
            except ValueError:
                raise NoMoreBeerException
            return res
        else:
            raise PassedOutException

    def get_beer(self, beer_id):
        # not implemented
        pass

    def give_me_beers(self, how_many, per_round):
        if how_many < 1:
            raise SoberNotAllowedException
        pages = math.floor(how_many / per_round) + \
            (0 if how_many % per_round == 0 else 1)
        for page in range(1, pages + 1):
            yield self.get_beers(page, per_round if how_many > per_round else how_many)


class PGDBManager():
    def __init__(self):
        self.connection = psycopg2.connect(DB_CONN)

    def insert(self, sql, params):
        with self.connection.cursor() as cursor:
            try:
                cursor.execute(sql, params)
                self.connection.commit()
                return cursor.fetchone()[0]
            except psycopg2.DatabaseError as err:
                # Eat up duplicate IDs
                if not err.pgcode == "23505":
                    print(err)
                self.connection.rollback()

        return None

    def fetch_one(self, sql, params=None):
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            return cursor.fetchone()

    def fetch_all_in_batch(self, sql, params=None, chunksize=1000):
        # iterative batch not implemented
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            return cursor.fetchmany(chunksize)


class BeerIsPunkDataManager(PGDBManager):
    def insert_beer(self, **kwargs):
        if kwargs.get("id") is None:
            print("Cannot insert without ID")
            return

        sql = """
            INSERT INTO 
            punk_is_beer
            (id, name, tagline, first_brewed, abv)
            VALUES
            (%s, %s, %s, %s, %s)
            """
        params = (
            kwargs.get("id"),
            kwargs.get("name", None),
            kwargs.get("tagline", None),
            kwargs.get("first_brewed", None),
            kwargs.get("abv", None),
        )
        return super().insert(sql, params)

    def get_abv_stats(self, how_many=10):
        sql = f"""
            SELECT 
            min(abv) as minimum_abv,
            max(abv) as maximum_abv,
            avg(abv) as average_abv
            FROM 
            punk_is_beer
            LIMIT {how_many}
            OFFSET 0
            """
        return super().fetch_one(sql)

    def get_food_pairings_stats(self, how_many=10):
        sql = f"""
            SELECT 
            min(abv) as minimum_abv,
            max(abv) as maximum_abv,
            avg(abv) as average_abv
            FROM 
            punk_is_beer
            LIMIT {how_many}
            OFFSET 0
            """
        return super().fetch_one(sql)

    def get_all_beers(self):
        sql = f"""
           SELECT
           *
           FROM
           punk_is_beer
           ORDER BY
           id ASC
            """
        return super().fetch_all_in_batch(sql)

    def insert_food_pairing(self, **kwargs):
        if kwargs.get("id") is None:
            print("Cannot insert without ID")
            return

        sql = """
            INSERT INTO 
            food_pairings
            (id, food)
            VALUES
            (%s, %s)
            """
        params = (
            kwargs.get("id"),
            kwargs.get("food")
        )
        return super().insert(sql, params)

    def get_avg_abv_of_food_pairing(self):
        sql = """
            WITH ranked_beer_food_pairings AS
            (SELECT id,
                    count(*) AS cnt
                FROM food_pairings
                GROUP BY id
                ORDER BY cnt DESC),
            maxed_food_pairings AS
            (SELECT max(cnt) AS maxcnt
            FROM ranked_beer_food_pairings)
            SELECT avg(b.abv)
            FROM punk_is_beer AS b
            CROSS JOIN maxed_food_pairings AS m
            JOIN ranked_beer_food_pairings AS fp ON fp.cnt = m.maxcnt
            AND b.id = fp.id
            """
        return super().fetch_one(sql)

    def get_beers_with_max_food_pairing(self):
        sql = """
            WITH ranked_beer_food_pairings AS
            (SELECT id,
                    count(*) AS cnt
                FROM food_pairings
                GROUP BY id
                ORDER BY cnt DESC),
            maxed_food_pairings AS
            (SELECT max(cnt) AS maxcnt
            FROM ranked_beer_food_pairings)
            SELECT b.name
            FROM punk_is_beer AS b
            CROSS JOIN maxed_food_pairings AS m
            JOIN ranked_beer_food_pairings AS fp ON fp.cnt = m.maxcnt
            AND b.id = fp.id
            """
        return super().fetch_all_in_batch(sql)


if __name__ == "__main__":
    airup = BeerIsPunk(rate_limit_knock_off_time=RATE_LIMIT_KNOCK_OFF_SEC)
    cellar = BeerIsPunkDataManager()
    bar = {}  # why fetch from the cellar when the bar is closer

    try:
        print("Brewing.....")
        for carton in airup.give_me_beers(ORDERS, BEER_PER_PAGE_LIMIT):
            for beer in carton:
                last_inserted_id = cellar.insert_beer(
                    id=beer.get("id"),
                    name=beer.get("name"),
                    tagline=beer.get("tagline"),
                    first_brewed=beer.get("first_brewed"),
                    abv=beer.get("abv")
                )
                if beer.get("id") not in bar:
                    bar[beer.get("id")] = beer
                if last_inserted_id is not None:
                    print(f"{beer.get('name')}::{beer.get('id')} brewed.")

    except ThatsNoBeerException:
        sys.exit("That don't seem like beer. Order a beer properly.")
    except NoMoreBeerException:
        print("You drank it all!!!!")
    except PassedOutException:
        print("Passed out!!!")
    except SoberNotAllowedException:
        sys.exit("Try a proper beer and get drunk.")

    NO_OF_ABV = 25
    abv_stats = cellar.get_abv_stats(NO_OF_ABV)
    print(f"The minimum of abv for {NO_OF_ABV} beer is {abv_stats[0]}")
    print(f"The maximum of abv for {NO_OF_ABV} beer is {abv_stats[1]}")
    print(f"The average of abv for {NO_OF_ABV} beer is {abv_stats[2]}")

    # Run this once and if the above didn't lead to an exception, as checks for duplication not done
    for beer in cellar.get_all_beers():
        if beer[0] in bar:
            nearby_beer = bar[beer[0]]
            for food in nearby_beer.get("food_pairing"):
                print(food)
                cellar.insert_food_pairing(
                    id=nearby_beer.get("id"),
                    food=food
                )
        else:
            # Not implemented: Should already be in bar. Increase ORDERS program param to get more
            pass

    print("Beer with maximum food pairings:", ", ".join(
        [beer[0] for beer in cellar.get_beers_with_max_food_pairing()]))
    print("The average abv of the beer with max pairings is",
          cellar.get_avg_abv_of_food_pairing()[0])
